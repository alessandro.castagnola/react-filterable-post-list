import React from 'react';
import Post, { IPostProps } from './Post';
import './List.css';
import { useFilterPosts } from './useFilterPosts';

interface IListProps {
    posts: IPostProps[];
    searchString: string;
    isLoading: boolean;
}

const List = ({ posts, searchString, isLoading }: IListProps) => {
    const { filtered, qty } = useFilterPosts(posts, searchString);

    const LoadingTemplate = () => {
        return (
            <div className="container container--info">
                <h3>Loading...</h3>
            </div>
        );
    };

    const NoPosts = () => {
        return (
            <div className="container container--info">
                <h3>No posts to show</h3>
            </div>
        );
    };

    const getTitle = (qty: number) => {
        return (
            <div className="container container--title">
                <h4 className="title">Post list #{qty}</h4>
            </div>
        );
    };

    return (
        <section className="container container--posts">
            {isLoading && <LoadingTemplate />}
            {!isLoading && !posts.length && <NoPosts />}
            {getTitle(qty)}
            {filtered.map((post: IPostProps) => (
                <Post key={post.id} {...post} />
            ))}
        </section>
    );
};

export default List;
