import React from "react";
import "./Post.css";
import { useState } from "react";

export interface IPostProps {
  id: number;
  title: string;
  body: string;
}

// Function params deconstructing
const Post = ({ title, body }: IPostProps): JSX.Element => {
  const [articleBody, setArticleBody] = useState(false);

  const handleBody = () => {
    setArticleBody(!articleBody);
  };

  return (
    <article
      className="container post"
      onClick={handleBody}
      onKeyDown={handleBody}
    >
      <div className="post__header">
        <p>{title}</p>
        <span className="post__button">&#10515;</span>
      </div>
      {articleBody && (
        <div className="post__body">
          <p>{body}</p>
        </div>
      )}
    </article>
  );
};

export default Post;
