import React from 'react';
import { useFetchPosts } from './fetch/useFetchPosts';
import Title from './Title';
import SearchPost from './SearchPost';
import List from './List';

export default () => {
    const { isLoading, posts, searchString, setSearchString } = useFetchPosts();

    return (
        <>
            <Title />
            <SearchPost setSearchString={setSearchString} />
            <List posts={posts} searchString={searchString} isLoading={isLoading} />
        </>
    );
};
