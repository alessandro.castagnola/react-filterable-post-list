import React from 'react';
import './SearchPost.css';

// TODO: improve any
interface ISearchProps {
    setSearchString: (searchString: string) => void 
}

export default ({ setSearchString }: ISearchProps) => {
    // TODO: useRef for focus
    // Are you kiddin' me :-) ?!? useRef for focus ?!? keep it simple, just "autofocus on input"

    return (
        <section className="container container--search">
            <input
                autoFocus
                placeholder="search post..."
                className="search__input"
                onChange={(event) => setSearchString(event.target.value)}
            />
        </section>
    );
};
