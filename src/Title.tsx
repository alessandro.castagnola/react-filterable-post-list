import React from 'react';
import './Title.css';

export default () => (
    <section className="container container--title">
        <h1>Filterable user list</h1>
    </section>
);
