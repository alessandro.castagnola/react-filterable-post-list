import { useState, useEffect } from "react";
import { IPostProps } from "../Post";

// TODO: improve any
export interface Response {
  isLoading: boolean;
  posts: IPostProps[];
  searchString: string
  setSearchString: (searchString: string) => void
}

export const useFetchPosts = (): Response => {
  const [isLoading, setIsLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const [searchString, setSearchString] = useState('');

  const getPosts = (url: string) => {
    fetch(url)
      .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
          return response.json();
        }
        throw new Error(response.status.toString());
      })
      .then((data) => {
        setPosts(data);
      })
      .catch((error) => {
        //if you catch the error why not manage it with a component ;-)
        console.error(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    getPosts('https://jsonplaceholder.typicode.com/posts');
  }, []);

  return {
    isLoading,
    posts,
    searchString,
    setSearchString
  };
};
