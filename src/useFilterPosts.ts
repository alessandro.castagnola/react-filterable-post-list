import { IPostProps } from './Post';

type FilterPosts = (
    posts: IPostProps[],
    searchString: string,
) => {
    filtered: IPostProps[];
    qty: number;
};

export const useFilterPosts: FilterPosts = (posts, searchString) => {
    const searchTerm = searchString.toLowerCase();
    const filtered = posts.filter((post) => post.title.includes(searchTerm) || post.body.includes(searchTerm));
    return {
        filtered,
        qty: filtered.length,
    };
};
